#!/bin/sh
# Copyright 2022 Helmut Grohne <helmut@subdivi.de>
# SPDX-License-Identifier: MIT

# shellcheck disable=SC2016 # Intentional quoting technique

: <<'POD2MAN'
=head1 NAME

debvm-create - Create a VM image for various Debian releases and architectures

=head1 SYNOPSIS

B<debvm-create> [B<-h> I<hostname>] [B<-k> F<sshkey>] [B<-o> F<output>] [B<-r> I<release>] [B<-s> <task>] [B<-z> I<size>] [B<--> I<mmdebstrap options>]

=head1 DESCRIPTION

B<debvm-create> is essentially a thin wrapper around B<mmdebstrap> for creating a raw ext4 filesystem image for booting with B<debvm-run>.
The purpose of these images primarily is testing the different releases and architectures without access to a physical machine of that architecture.
Beyond essential packages, the image will contain B<apt>, an init system and a suitable kernel package.
Notably absent is a bootloader and a partition table.
In order to boot such an image, one is supposed to extract the kernel and initrd from the image and pass it to a suitable bootloader.
No user account is created and root can login without specifying a password.

=head1 OPTIONS

=over 8

=item B<-h> I<hostname>, B<--hostname>=I<hostname>

Set the hostname of the virtual machine.
By default, the hostname is B<testvm>.

=item B<--initsystem>=B<systemd> | B<busybox> | B<finit> | B<runit> | B<sysv> | B<none>

Select an init system to be used.
The default is B<systemd> independently of the Debian release.
Note that when selecting B<none>, the resulting image will not be bootable unless something else takes care of creating F</sbin/init>.
Automatic customizations that are specific to a particular init system will be skipped when a different init system is selected.
When using the B<finit> init system consider passing B<--include=finit-plugins> to B<mmdebstrap>.

=item B<-k> F<sshkey>, B<--sshkey>=F<sshkey>

Install the given ssh public key file into the virtual machine image for the root user.
This option also causes the ssh server to be installed.
By default, no key or server is installed.
To connect to the vm, pass a port number to B<debvm-run> with the B<-s> option.

=item B<-o> F<output>, B<--output>=F<output>

Specify the file name of the resulting virtual machine image.
By default, it is written to F<rootfs.ext4>.

=item B<-r> I<release>, B<--release>=I<release>

Use the given Debian release.
By default, B<unstable> is being used.

=item B<-s> I<task>, B<--skip>=I<task>

Skip a particular task or feature.
The option may be specified multiple times or list multiple tasks to be skipped by separating them with a comma.
By default, no tasks are skipped.
The following tasks may be skipped.

=over 4

=item B<autologin>

Skips adding a the customize-autologin.sh to B<mmdebstrap> that configures
automatic root login on a serial console and also parses the C<TERM> kernel
cmdline and passes it as C<TERM> to B<agetty>.
This is specific to using B<finit>, B<runit>, B<systemd> or B<sysv> as init system.

=item B<ext4>

Internally, B<mmdebstrap> creates a tar archive first and converts that to ext2, which is then upgraded to ext4.
This option causes the conversion to ext2 and further steps to be skipped and the output image will be a tar archive instead.
Such a tar archive is not suitable for being booted by B<debvm-run>.

=item B<ifupdown>

skips installing B<ifupdown> configuration to automatically configure wired interfaces.
This is specific to using B<finit>, B<runit> or B<sysv> as init system.

=item B<initsystem>

skips installing an init system.
This is equivalent to specifying B<--initsystem=none>.

=item B<kernel>

skips installing a linux kernel image.
This can be useful to install a kernel without a package.
If a kernel is installed via B<mmdebstrap> option C<--include>, automtatic kernel installation is automatically skipped.

=item B<packagelists>

reduces the package lists inside the image.
The B<available> database for B<dpkg> is not created.
The package lists used by B<apt> are deleted.
This generally produces a smaller image, but you need to run B<apt update> before installing packages and B<dpkg --set-selections> does not work.

=item B<systemdnetwork>

skips installing B<libnss-resolve> as well as automatic network configuration via B<systemd-networkd>.
This is specific to using B<systemd> as init system.

=item B<usrmerge>

By default B<debvm> adds a hook to enable merged-/usr without the B<usrmerge> package given a sufficiently recent Debian release.
Without the hook, dependencies will pull the B<usrmerge> package as needed, which may result in a larger installation.

=back

=item B<-z> I<size>, B<--size>=I<size>

Specify the minimum image size as an integer and optional unit (example: 10K is 10*1024).
Units are K,M,G,T,P,E,Z,Y (powers of 1024) or KB,MB,... (powers of 1000).
The resulting image will be grown as a sparse file to this size if necessary.
The default is 1 GB.

=item B<--> I<mmdebstrap options>

All options beyond a double dash are passed to B<mmdebstrap> after the suite and target specification.
This can be used to provide additional hooks for image customization.
You can also request additional packages to be installed into the image using B<mmdebstrap>'s B<--include> option.
Any positional arguments passed here will be treated as mirror specifications by B<mmdebstrap>.
In particular, you can also change the architecture of the resulting image using the B<--architecture> option.

=back

=head1 EXAMPLES

When creating an image with multiple architectures, the kernel selection will prefer the sibling 64bit architecture.

    debvm-create ... -- --architecture=armhf,arm64

In order to create images for Debian ports architectures, you can pass two options to mmdebstrap.

    debvm-create ... -- http://deb.debian.org/debian-ports --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg

You can also install a graphical desktop environment.

    debvm-create ... -- --hook-dir=/usr/share/mmdebstrap/hooks/useradd --aptopt='Apt::Install-Recommends "true"' --include=linux-image-generic,task-gnome-desktop

Here the hook creates a password-less user C<user>.
In order for C<task-gnome-desktop> to work reasonably well, C<Recommends> should be enabled.
By default a C<-cloud> kernel that lacks graphics drivers is installed.

Installing Ubuntu is also supported somewhat.

    debvm-create --release kinetic -- --keyring=/usr/share/keyrings/ubuntu-archive-keyring.gpg --components=main,universe --include=e2fsprogs

Note that C<universe> must be enabled as the C<merged-usr> and C<systemdnetwork> hooks rely packages from that component.
C<e2fsprogs> should be pulled by C<initramfs-tools>.

There also is an aid for sharing a directory with the VM.

    debvm-create ... -- --include=linux-image-generic --hook-dir=/usr/share/mmdebstrap/hooks/9pmount
    debvm-run ... -- -virtfs local,security_model=none,path=/host_path,mount_tag=guest_tag

This will mount the directory C</host_path> from the host as C</media/guest_tag> in the VM during boot using 9P.
Note that the C<-cloud> kernel lacks the 9P filesystem driver.

=head1 SEE ALSO

    debvm-run(1) mmdebstrap(1)

=cut
POD2MAN

set -u

ARCHITECTURE=
IMAGE=rootfs.ext4
INITSYSTEM=systemd
SIZE=1G
SKIP=,
SSHKEY=
SUITE=unstable
VMNAME=testvm

SHARE_DIR="${0%/*}/../share"

nth_arg() {
	shift "$1"
	printf "%s" "$1"
}

die() {
	echo "$*" 1>&2
	exit 1
}
usage() {
	die "usage: $0 [-h hostname] [-k sshkey] [-o output] [-r release] [-s task] [-z size] [-- mmdebstrap options]"
}
usage_error() {
	echo "error: $*" 1>&2
	usage
}

opt_architecture() {
	# This option only exists for backwards-compatibility.
	# You can pass it as mmdebstrap option instead.
	ARCHITECTURE=$1
}
opt_hostname() {
	VMNAME=$1
}
opt_initsystem() {
	case "$1" in
		busybox|finit|none|runit|systemd|sysv)
		;;
		*)
			die "value for --initsystem must be one of systemd, busybox, finit, none, runit or sysv"
		;;
	esac
	INITSYSTEM=$1
}
opt_skip() {
	if test "$1" = initsystem; then
		opt_initsystem none
	else
		SKIP="$SKIP$1,"
	fi
}
opt_sshkey() {
	SSHKEY=$1
}
opt_output() {
	IMAGE=$1
	test "${IMAGE#-}" = "$IMAGE" || IMAGE="./$IMAGE"
}
opt_release() {
	SUITE=$1
}
opt_size() {
	SIZE=$1
}

while getopts :a:h:k:o:r:s:z:-: OPTCHAR; do
	case "$OPTCHAR" in
		a)	opt_architecture "$OPTARG"	;;
		h)	opt_hostname "$OPTARG"		;;
		k)	opt_sshkey "$OPTARG"		;;
		o)	opt_output "$OPTARG"		;;
		r)	opt_release "$OPTARG"		;;
		s)	opt_skip "$OPTARG"		;;
		z)	opt_size "$OPTARG"		;;
		-)
			case "$OPTARG" in
				help)
					usage
				;;
				architecture|hostname|initsystem|output|release|size|skip|sshkey)
					test "$OPTIND" -gt "$#" && usage_error "missing argument for --$OPTARG"
					"opt_$OPTARG" "$(nth_arg "$OPTIND" "$@")"
					OPTIND=$((OPTIND+1))
				;;
				architecture=*|hostname=*|initsystem=*|output=*|release=*|size=*|skip=*|sshkey=*)
					"opt_${OPTARG%%=*}" "${OPTARG#*=}"
				;;
				*)
					usage_error "unrecognized option --$OPTARG"
				;;
			esac
		;;
		:)
			usage_error "missing argument for -$OPTARG"
		;;
		'?')
			usage_error "unrecognized option -$OPTARG"
		;;
		*)
			die "internal error while parsing command options, please report a bug"
		;;
	esac
done
shift "$((OPTIND - 1))"

if test -n "$SSHKEY" && ! test -f "$SSHKEY"; then
	die "error: ssh keyfile '$SSHKEY' not found"
fi

check_skip() {
	case "$SKIP" in
		*",$1,"*)	return 0 ;;
		*)		return 1 ;;
	esac
}

if ! check_skip kernel; then
	set -- "--customize-hook=$SHARE_DIR/customize-kernel.sh" "$@"
fi

MMFORMAT=ext2
# output a tarball if the ext4 step is skipped
if check_skip ext4; then
	MMFORMAT=tar
fi

# construct mmdebstrap options as $@:
set -- \
	--verbose \
	--variant=apt \
	"--format=$MMFORMAT" \
	'--customize-hook=echo "LABEL=debvm / ext4 defaults 0 0" >"$1/etc/fstab"' \
	"$@"

if test -n "$ARCHITECTURE"; then
	set -- "--architecture=$ARCHITECTURE" "$@"
fi

case "$INITSYSTEM" in
	busybox)
		set -- \
			--include=busybox \
			'--customize-hook=ln -s /bin/busybox $1/sbin/init' \
			"$@"
		SKIP="${SKIP}autologin,"
	;;
	finit)
		set -- --include=finit-sysv,mount "$@"
	;;
	none)
		SKIP="${SKIP}autologin,"
	;;
	runit)
		set -- --include=runit-init,passwd "$@"
	;;
	systemd)
		set -- --include=systemd-sysv "$@"
	;;
	sysv)
		set -- \
			--include=sysvinit-core \
			'--include=?not(?virtual)?exact-name(orphan-sysvinit-scripts)' \
			"$@"
	;;
esac

# set up a hostname
set -- \
	"--customize-hook=echo $VMNAME >"'"$1/etc/hostname"' \
	"--customize-hook=echo 127.0.0.1 localhost $VMNAME >"'"$1/etc/hosts"' \
	"$@"

# allow password-less root login
set -- '--customize-hook=passwd --root "$1" --delete root' "$@"

if test "$INITSYSTEM" = systemd && ! check_skip systemdnetwork; then
	# dhcp on all network interfaces, and add a dns resolver
	set -- \
		"--customize-hook=$SHARE_DIR/customize-networkd.sh" \
		'--include=?not(?virtual)?exact-name(libnss-resolve)' \
		"--customize-hook=$SHARE_DIR/customize-resolved.sh" \
		"$@"
elif test "$INITSYSTEM" = sysv -o "$INITSYSTEM" = runit -o "$INITSYSTEM" = finit && ! check_skip ifupdown; then
	set -- \
		'--include=ifupdown,isc-dhcp-client' \
		"--customize-hook=$SHARE_DIR/customize-ifupdown.sh" \
		"$@"
fi

# add ssh key for root
if test -n "$SSHKEY"; then
	set -- \
		--include=openssh-server \
		'--customize-hook=mkdir -m700 -p "$1/root/.ssh"' \
		"--customize-hook=upload $SSHKEY /root/.ssh/authorized_keys" \
		"$@"
fi

if ! check_skip packagelists; then
	set -- --skip=cleanup/apt/lists "$@"
	set -- "--customize-hook=$SHARE_DIR/customize-dpkgavailable.sh" "$@"
fi

if test "$SUITE" = jessie; then
	# Use obsolete and expired keys.
	set -- '--keyring=/usr/share/keyrings/debian-archive-removed-keys.gpg' "$@"
	set -- --aptopt='Apt::Key::gpgvcommand "/usr/libexec/mmdebstrap/gpgvnoexpkeysig"' "$@"
	set -- --hook-dir=/usr/share/mmdebstrap/hooks/jessie-or-older "$@"
fi

if ! check_skip usrmerge; then
	# Avoid the usrmerge package
	set -- --hook-dir=/usr/share/mmdebstrap/hooks/maybe-merged-usr "$@"
fi

if ! check_skip autologin; then
	set -- "--customize-hook=$SHARE_DIR/customize-autologin.sh" "$@"
fi

set -- "$SUITE" "$IMAGE" "$@"

set -ex

mmdebstrap "$@"

{ set +x; } 2>/dev/null
check_skip ext4 && exit

set -x

truncate -s ">$SIZE" "$IMAGE"
/sbin/resize2fs "$IMAGE"
/sbin/tune2fs -L debvm -c 0 -i 0 -O dir_index,dir_nlink,extents,extra_isize,flex_bg,has_journal,huge_file "$IMAGE"
/sbin/resize2fs -b "$IMAGE"
# Must fsck after tune2fs: https://ext4.wiki.kernel.org/index.php/UpgradeToExt4
/sbin/fsck.ext4 -fDp "$IMAGE"
