#!/bin/sh
# Copyright 2022 Helmut Grohne <helmut@subdivi.de>
# SPDX-License-Identifier: MIT
#
# This is a mmdebstrap customize hook that initializes dpkg's available
# database from an updated apt package list cache.
#
# Without the available database, dpkg --set-selections won't work.

set -eu

APT_CONFIG=$MMDEBSTRAP_APT_CONFIG apt-cache dumpavail | dpkg --root "$1" --update-avail
