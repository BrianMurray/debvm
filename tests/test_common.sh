#!/bin/sh

run_ssh() {
	test -n "${SSH_KEYPATH:-}" && set -- -i "$SSH_KEYPATH" "$@"
	test -n "${SSH_PORT:-}" && set -- -p "$SSH_PORT" "$@"
	ssh -o NoHostAuthenticationForLocalhost=yes -l root "$@"
}
